## Synopsis

This is a custom view that will draw a customizable checkmark with animation (or not) on the center of the view. We can change the color, the length of the arms, the duration of the animation. Here are a few of the checkmarks that you could generate, all of those with customizable times for the animation

![checkmarks.png](https://bitbucket.org/repo/7ARzAR/images/2817596559-checkmarks.png)
## Code Example

    <coop.devtopia.opensource
        android:layout_width="250dp"
        android:layout_height="250dp"
        android:layout_centerInParent="true"
        android:id="@+id/view"

        app:first_leg_length="100"
        app:second_leg_length="20"
        app:total_duration="1500"
        app:stroke_width="8"
        app:stroke_color="#FF0000"/>

The previous code will create a normal, fully opaque, red checkmark that will have a width of 8dp and it will draw it during 1.5 seconds

## Motivation

I needed it for a project and since I had to create it, I'd like to save someone the hassle of **developing** it again.

## Installation

 - Add the view class
 - Add the attrs.xml file or append to your own file
 - Fix imports and package names
 - Enjoy!

## More

If you feel comfortable with editing the view itself (and you should) you can easily change the interpolator for the animation. To change from the original bounce interpolator to something else, I think overshoot looks quite nice too.

## To come

I'll be moving this to gradle so you can import and use it without downloading manually, but it still needs some work before it's worth the trouble.


## License

https://creativecommons.org/licenses/by/4.0/