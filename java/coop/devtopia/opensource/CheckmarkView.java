package coop.devtopia.opensource;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;


/**
 * Custom view that will draw a customized (to some extent) checkmark that can be anywhere in your
 * interface. It was designed to be placed as an overlay of other views, such as an ImageView or
 * VideoView. A feedback to the user clicking something.
 *
 * Author: juan.cortes@devtopia.coop
 */
public class CheckmarkView extends View {
    //Static strings and configurable variables (not from xml)
    private final static String FIRST_LEG_LENGTH = "first_leg_length";
    private final static String SECOND_LEG_LENGTH = "second_leg_length";
    private final static Double FIRST_LEG_TIME_PORTION = 0.4;
    private final static Double SECOND_LEG_TIME_PORTION = 0.6;
    private final static Float ZERO = 0f;

    //Editable Properties
    protected float first_leg_length = 50;
    protected float second_leg_length = 100;
    protected int stroke_width = 20;
    protected int total_duration = 1500;
    protected int stroke_color = Color.BLACK;


    protected float mPlaceHolderFirstLegLength = 50;
    protected float mPlaceHolderSecondLegLength = 100;
    protected Paint mPaint;

    protected ObjectAnimator mFirstLegAnimator, mSecondLegAnimator;
    protected DisplayMetrics mMetrics = new DisplayMetrics();

    //Flags
    private boolean shouldAnimateFirst = false;
    private boolean shouldAnimateSecond = false;


    public CheckmarkView(Context context) {
        super(context);
    }

    public CheckmarkView(Context context, AttributeSet set) {
        super(context, set);
        TypedArray a = context.obtainStyledAttributes(set, R.styleable.check_mark_view);

        //Get properties out of the xml attributes
        Float firstLegLength = a.getFloat(R.styleable.check_mark_view_first_leg_length, this.first_leg_length);
        Float secondLegLength = a.getFloat(R.styleable.check_mark_view_second_leg_length, this.second_leg_length);
        Integer totalDuration = a.getInt(R.styleable.check_mark_view_total_duration, this.total_duration);
        Integer strokeWidth = a.getInt(R.styleable.check_mark_view_stroke_width, this.stroke_width);
        Integer strokeColor = a.getColor(R.styleable.check_mark_view_stroke_color, this.stroke_color);

        //Set properties for this instance
        if (firstLegLength != null) {
            this.first_leg_length = firstLegLength;
            mPlaceHolderFirstLegLength = firstLegLength;
        }

        if (secondLegLength != null) {
            this.second_leg_length = secondLegLength;
            mPlaceHolderSecondLegLength = secondLegLength;
        }

        if (strokeWidth != null) {
            stroke_width = strokeWidth;
        }

        if (strokeColor != null) {
            stroke_color = strokeColor;
        }

        if (totalDuration != null) {
            total_duration = totalDuration;
        }

        a.recycle();

        init(context);
    }

    private void init(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(mMetrics);

        stroke_width = (int) (stroke_width * mMetrics.density);
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(stroke_width);
        mPaint.setColor(stroke_color);
    }

    public CheckmarkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void resetAnimation(){
        first_leg_length = ZERO;
        second_leg_length = ZERO;

        shouldAnimateFirst = true;
        shouldAnimateSecond = false;

        mSecondLegAnimator = ObjectAnimator.ofFloat(this, FIRST_LEG_LENGTH, ZERO, mPlaceHolderFirstLegLength * mMetrics.density);
        mSecondLegAnimator.setDuration((long)(total_duration * FIRST_LEG_TIME_PORTION));
        mSecondLegAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mSecondLegAnimator.start();


        mFirstLegAnimator = ObjectAnimator.ofFloat(this, SECOND_LEG_LENGTH, ZERO, mPlaceHolderSecondLegLength * mMetrics.density);
        mFirstLegAnimator.setDuration((long)(total_duration * SECOND_LEG_TIME_PORTION));
        mFirstLegAnimator.setInterpolator(new BounceInterpolator());
    }

    public void start(){
        resetAnimation();
    }

    float[] offsets = new float[]{0,0};
    private void calculateOffsets(Canvas canvas){
        if(offsets[0] == 0 || offsets[1] == 0){
            //Calculate the bounding box for the path
            float width = (first_leg_length + second_leg_length) * mMetrics.density;
            float height = Math.max(second_leg_length * mMetrics.density,first_leg_length * mMetrics.density);

            //Calculate starting position for the first leg (second is inherited)
            boolean isFirstLegLongest = (first_leg_length >= second_leg_length);

            //Calculate intrinsic offset (centering within view)
            offsets[0] = (canvas.getWidth() - width) / 2 ;
            offsets[1] = (canvas.getHeight() - height) / 2 ;

            //Adjust the offset for
            if(!isFirstLegLongest){
                offsets[1] += (second_leg_length - first_leg_length) * mMetrics.density;
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        calculateOffsets(canvas);

        if(shouldAnimateFirst){
            Path p = new Path();

            p.moveTo(offsets[0], offsets[1]);
            p.lineTo(offsets[0] + first_leg_length, offsets[1] + first_leg_length);

            if(shouldAnimateSecond){
                p.moveTo(offsets[0]+ first_leg_length, offsets[1] + first_leg_length-(stroke_width/3*2.1f));
                p.lineTo(offsets[0] + first_leg_length + second_leg_length, offsets[1] + first_leg_length - second_leg_length -(stroke_width/3*2.1f));
            }
            canvas.drawPath(p, mPaint);
        }
    }



    /**
     * Getter and setter for the animated length of the first leg
     * @return
     */
    public float getFirst_leg_length() {
        return first_leg_length;
    }

    public void setFirst_leg_length(float first_leg_length) {
        this.first_leg_length = first_leg_length;
        if(first_leg_length == mPlaceHolderFirstLegLength * mMetrics.density){
            mFirstLegAnimator.start();
            shouldAnimateSecond = true;
        }
        this.invalidate();
    }

    /**
     * Getter and setter for the animated length of the second leg
     * @return
     */
    public float getSecond_leg_length() {
        return second_leg_length;
    }

    public void setSecond_leg_length(float second_leg_length) {
        this.second_leg_length = second_leg_length;
        this.invalidate();
    }

}